const hour = document.getElementById("hour");
const minutes = document.getElementById("minutes");
const seconds = document.getElementById("seconds");

function time() {
  let dateToday = new Date();
  let hr = dateToday.getHours();
  let min = dateToday.getMinutes();
  let sec = dateToday.getSeconds();

  hour.textContent = hr < 10 ? '0' + hr : hr;
  minutes.textContent = min < 10 ? '0' + min : min;
  seconds.textContent = sec < 10 ? '0' + sec : sec;
}

var clock = setInterval(time, 1000);

/* 
  
  CRIEI AS CONSTANTES PEGANDO CADA ID DO SPAN COM O GET ELEMENT
  CRIEI UMA FUNÇÃO TIME ONDE PEGA A CLASSE DATE()
  APÓS ISSO ADICIONANDO CADA CONSTANTE A VARIAÇÃO DO TEMPO COM UMA CONDIÇÃO TERNÁRIA PARA ADICIONAR O ZERO A ESQUERDA
  JUNTAMENTE COM O INTERVALO DE ATUALIZAÇÃO

*/
